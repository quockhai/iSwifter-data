##### What are the states of an iOS App?
***

1. **Non-running** - The app is not running.
2. **Inactive** - The app is running in the foreground, but not receiving events. An iOS app can be placed into an inactive state, for example, when a call or SMS message is received.
3. **Active** - The app is running in the foreground, and receiving events.
4. **Background** - The app is running in the background, and executing code.
5. **Suspended** - The app is in the background, but no code is being executed.