##### What is GraphQL?
***

**GraphQL** is trying to solve creating a query interface for the clients at the application level. **[Apollo iOS](https://github.com/apollographql/apollo-ios)** is a strongly-typed, caching GraphQL client for iOS, written in Swift.