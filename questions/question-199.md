##### Explain how to add frameworks in Xcode project?
***

* First, choose the project file from the project navigator on the left side of the project window
* Then choose the target where you want to add frameworks in the project settings editor
* Choose the “Build Phases” tab, and select “Link Binary With Libraries” to view all of the frameworks
* To add frameworks click on “+” sign below the list select framework and click on add button.