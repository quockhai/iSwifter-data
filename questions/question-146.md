##### What is Protocol Extensions?
***

We can adopt protocols using extensions as well as on the original type declaration. This allows you to add protocols to types you don’t necessarily own.