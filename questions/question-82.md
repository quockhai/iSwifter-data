##### What is the difference between a delegate and an NSNotification?
***

Delegates and NSNotifications can be used to accomplish nearly the same functionality. However, delegates are one-to-one while NSNotifications are one-to-many.