##### Explain Neural networks with CoreML
***

Neural networks and deep learning currently provide the best solutions to many problems in image recognition, speech recognition, and natural language processing.

CoreML is an iOS framework comes with iOS 11, helps to process models in your apps for face detection. For more information follow this [guideline](https://developer.apple.com/machine-learning/).