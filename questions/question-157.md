##### Explain unwind segue
***

An [unwind segue](https://developer.apple.com/library/archive/referencelibrary/GettingStarted/DevelopiOSAppsSwift/GlossaryDefinitions.html#//apple_ref/doc/uid/TP40015214-CH12-SW73) moves backward through one or more segues to return the user to a scene managed by an existing view controller.