##### Explain UNNotification Content
***

UNNotification Content stores the notification content in a scheduled or delivered notification. It is read-only.