##### Explain #keyPath()
***

Using #keyPath(), a static type check will be performed by virtue of the key-path literal string being used as a StaticString or StringLiteralConvertible. At this point, it’s then checked to ensure that it:

* is actually a thing that exists and
* is properly exposed to Objective-C.