##### What is Enum or Enumerations?
***

According to Apple’s Swift documentation:
> Managing state, the bits of data that keep track of how the app is being used at the moment, is an important part of a developing your app. Because enumerations define a finite number of states, and can bundle associated values with each individual state, you can use them to model the state of your app and its internal processes.

Enum is a type that basically contains a group of related values in the same umbrella but case-less enum won’t allow us to create an instance.

Reference: [Apple documentation](https://developer.apple.com/documentation/swift/maintaining_state_in_your_apps)