##### What is the disadvantage to hard-coding log statements?
***

**First**, when you start to log. This starts to accumulate. It may not seem like a lot, but every minute adds up. By the end of a project, those stray minutes will equal to hours.

**Second**, Each time we add one to the code base, we take a risk of injecting new bugs into our code.