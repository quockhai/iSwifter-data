##### Explain SOAP and REST Basics differences
***

Both of them helps us access Web services. **[SOAP](https://en.wikipedia.org/wiki/SOAP)** relies exclusively on XML to provide messaging services. SOAP is definitely the heavyweight choice for Web service access. Originally developed by Microsoft.

**[REST](https://en.wikipedia.org/wiki/Representational_state_transfer)** (Representational State Transfer) provides a lighter weight alternative. Instead of using XML to make a request, REST relies on a simple URL in many cases. REST can use four different HTTP 1.1 verbs (GET, POST, PUT, and DELETE) to perform tasks.