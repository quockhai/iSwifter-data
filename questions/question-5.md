##### What is the bounding box?
***

The bounding box is a term used in geometry; it refers to the smallest measure (area or volume) within which a given set of points.