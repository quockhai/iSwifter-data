##### Explain type inference
***

Swift uses type inference to work out the appropriate type. Type inference enables a compiler *to deduce the type of a particular expression automatically when it compiles your code*, simply by examining the values you provide.