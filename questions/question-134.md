##### What is pair programming?
***

Pair programming is a tool to share information with junior developers. Junior and senior developer sitting side-by-side this is the best way for the junior to learn from senior developers.

Check out Martin Fowler on [“Pair Programming Misconceptions”](https://martinfowler.com/bliki/PairProgrammingMisconceptions.html), WikiHow on [Pair Programming](https://www.wikihow.com/Pair-Program)