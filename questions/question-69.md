##### What is the benefit of writing tests in iOS apps?
***

* Writing tests first gives us a clear perspective on the API design, by getting into the mindset of being a client of the API before it exists.
* Good tests serve as great documentation of expected behavior.
* It gives us confidence to constantly refactor our code because we know that if we break anything our tests fail.
* If tests are hard to write its usually a sign architecture could be improved. Following RGR ( Red - Green - Refactor ) helps you make improvements early on.