##### Explain Viper Architecture
***

Viper is an another design patters. It has five layers: View, Interactor, Presenter, Entity, and Router. It is based on [Single Responsibility Principle](https://en.wikipedia.org/wiki/Single_responsibility_principle).

Advantages of Viper architecture is communication from one entity to another is given through protocols. The idea is to isolate our app’s dependencies, balancing the delegation of responsibilities among the entities.

You can find more details in this [article](https://swifting.io/blog/2016/09/07/architecture-wars-a-new-hope/).