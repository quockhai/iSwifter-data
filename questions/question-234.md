##### Explain Dependency Injection Container
***

The container keeps a map of each class type to an instance of that class. We can then instantiate any class by simply providing the type to the container. The container then automatically provides dependencies for the class.