##### Explain Common features of Protocols & superclasses
***

* implementation reuse
* provide points for customization
* interface reuse
* supporting modular design via dynamic dispatch on reused interfaces