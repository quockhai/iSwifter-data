##### What’s accessibilityHint?
***

`accessibilityHint` describes the results of interacting with a user interface element. A hint should be supplied only if the result of an interaction is not obvious from the element’s label.