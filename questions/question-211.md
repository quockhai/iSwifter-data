##### What is Sequence protocol?
***

A sequence is a list of values that we can step through one at a time. The most common way to iterate over the elements of a sequence is to use a [for-in loop](https://swiftdoc.org/v3.0/protocol/sequence/).