##### Explain Method Swizzling in Swift
***

Method Swizzling is a well-known practice in Objective-C and in other languages that support dynamic method dispatching.

Through swizzling, the implementation of a method can be replaced with a different one at runtime, by changing the mapping between a specific #selector(method) and the function that contains its implementation.

To use method swizzling with your Swift classes there are two requirements that you must comply with:

* The class containing the methods to be swizzled must extend NSObject.
* The methods you want to swizzle must have the dynamic attribute.