##### What is the difference between property and instance variable?
***

A property is a more abstract concept. An instance variable is literally just a storage slot, as a slot in a struct. Normally other objects are never supposed to access them directly. Usually, a property will return or set an instance variable, but it could use data from several or none at all.