##### What is the difference Stack and Heap?
***

Our code takes up some space in the iOS. The size of this is sometimes fixed and sometimes it can change according to what the user will enter during the program. Basically we have two different methods because of this difference: **Stack** and **Heap**

Stack is used and automatically removes itself from memory after work is finished. But in Heap the user could do it by writing manual code for deleting from memory.

**Stack**:

* Stack is easy to use.
* It’s kept in RAM on the computer.
* Created variables are automatically deleted when they exit the stack.
* It is quite fast compared to Heap.
* Constructed variables can be used without a pointer.

**Heap**:

* Compared to Stack, it is quite slow.
* It creates memory problems if not used correctly.
* Variables are used with pointers.
* It is created at runtime.