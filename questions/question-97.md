##### Explain the difference between SDK and Framework
***

SDK is a set of software development tools. This set is used for the creation of applications. A framework is basically a platform which is used for developing software applications. It provides the necessary foundation on which the programs can be developed for a specific platform. SDK and Framework complement each other, and SDKs are available for frameworks.