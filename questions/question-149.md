##### What is Remote Notifications attachment's limits?
***

We can be sent with video or image with push notification. But maximum payload is 4kb. If we want to sent high quality attachment, we should use **Notification Service Extension**.