##### Explain lazy in Swift
***

An initial value of the `lazy` stored properties is calculated only when the property is called for the first time. There are situations when the `lazy` properties come very handy to developers.