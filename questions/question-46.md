##### NSOperation - NSOperationQueue - NSBlockOperation
***

* **NSOperation**, adds a little extra overhead compared to GCD, but we can add dependency among various operations and re-use, cancel or suspend them.
* **NSOperationQueue**, it allows a pool of threads to be created and used to execute NSOperations in parallel. Operation queues aren’t part of GCD.
* **NSBlockOperation**, allows you to create an NSOperation from one or more closures. NSBlockOperations can have multiple blocks, that run concurrently.