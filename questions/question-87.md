##### What is the difference between LLVM and Clang?
***

[Clang](https://en.wikipedia.org/wiki/Clang) is the front end of LLVM tool chain [(“clang” C Language Family Frontend for LLVM)](http://clang.llvm.org/). Every [Compiler](https://en.wikipedia.org/wiki/Compiler#Front_end) has three parts:

1. Front end (lexical analysis, parsing).
2. Optimizer (Optimizing abstract syntax tree).
3. Back end (machine code generation).

> Front end ( Clang ) takes the source code and generates abstract syntax tree ( LLVM IR ).