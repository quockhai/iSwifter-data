##### What is the difference ANY and ANYOBJECT?
***

According to Apple’s Swift documentation:

* **Any** can represent an instance of any type at all, including function types and optional types.
* **AnyObject** can represent an instance of any class type.

Check out for [more details](https://medium.com/@mimicatcodes/any-vs-anyobject-in-swift-3-b1a8d3a02e00).