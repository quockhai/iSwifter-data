##### What is the difference between Array vs NSArray?
***

Array can only hold one type of data, NSArray can hold different types of data. The array is value type, NSArray is immutable reference type.