##### What is circular dependencies?
***

A **circular dependency** is a relation between two or more modules which either directly or indirectly depend on each other to function properly. Such modules are also known as mutually recursive.