##### Explain the difference between Generics and AnyObject in Swift
***

Generics are type safe, meaning if you pass a string as a generic and try to use as a integer the compiler will complain and you will not be able to compile your. Because Swift is using **Static typing**, and is able to give you a **compiler error**.

If you use AnyObject, the compiler has no idea if the object can be treated as a String or as an Integer. It will allow you to do whatever you want with it.