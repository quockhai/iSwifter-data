##### What is CoreSpotlight?
***

CoreSpotlight allows us to index any content inside of our app. While NSUserActivity is useful for saving the user’s history, with this API, you can index any data you like. It provides access to the CoreSpotlight index on the user’s device.