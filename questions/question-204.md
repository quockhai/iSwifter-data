##### How do we download images?
***

With URLSession, we can download an image as a data then convert it from NSData to UIImage lastly we connect it UIImageView IBOutlet. Better way is to use a [library](https://medium.com/ios-os-x-development/best-image-download-extension-library-for-swift-3-cf64ec1f84a0). Also with URLSession Adaptable Connectivity API we can built-in connectivity monitoring and run a request if there is no connection. The request will wait and download whenever the resource is available instead of failing.