##### Explain VIP (Clean-Swift) Architecture
***

**ViewController** interacts directly with an **Interactor** by sending **Requests** to it. The Interactor responds to those requests by sending a **Response** with data model to a **Presenter**. The Presenter formats data to be displayed, creates a **ViewModel** and notifies the **ViewController** that it should update its View based on the **ViewModel**. **ViewController** decides when the navigation to a different scene should happen by calling a method on a Router. The Router performs setup of the next View Controller and deals with wiring, passing data and delegation setup.

When compared to **VIPER**, difference is that the **ViewController** itself contacts **Router** for navigation.

Please check out this [project](https://github.com/ustwo/vip-demo-swift).