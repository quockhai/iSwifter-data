##### What is Keychain?
***

Keychain is an API for persisting data securly in iOS App. It is **secured using a hardware module** and is backed up to iCloud. I highly recomend a good library - **[Locksmith](https://github.com/matthewpalmer/Locksmith)**