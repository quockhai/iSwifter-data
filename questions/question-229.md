##### What is DTO?
***

DTO is widely used in web projects. It declares the response to the `Mappable` protocol. It contains the implementation of the `mapping(map: Map)` function.