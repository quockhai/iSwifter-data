##### What is difference Layout Margins and Directional Layout Margins?
***

**Layout Margins** property of a UIView is of type UIEdgeInsets and defines the top, left, bottom and right insets that when applied to the view’s frame define the view’s margin.

**Directional Layout Margins** that are aware of Right-To-Left (RTL) languages. This follows the pattern used when creating constraints with layout anchors. See this earlier post about RTL Languages.