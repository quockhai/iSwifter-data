##### Explain IGListKit
***

**IGListKit** provides automatically diff objects to create insertions, deletions, and moves before performing batch updates on the collection view. If a user deletes an update in the queue we’re viewing, we’ll see it fade out without requiring a pull down to refresh.

We say goodbye to UICollectionViewDatasource, and instead use an IGListAdapter with a IGListAdapterDataSource. The data source doesn’t return counts or cells, instead it provides an array of Section Controllers. Section Controllers are then used to configure & control cells within the given collection view section.