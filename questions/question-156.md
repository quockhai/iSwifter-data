##### Are you using CharlesProxy? Why/Why not?
***

If I need a proxy server that includes both complete requests and responses and the HTTP headers then I am using [CharlesProxy](https://www.raywenderlich.com/154244/charles-proxy-tutorial-ios). With CharlesProxy, we can support binary protocols, rewriting and traffic throttling.