##### What is the difference between BDD and TDD?
***

The main difference between BDD and TDD is the fact that BDD test cases can be read by non-engineers, which can be very useful in teams.

iOS I prefer [Quick](https://github.com/Quick/Quick) BDD framework and its “matcher framework,” called [Nimble](https://github.com/Quick/Nimble).