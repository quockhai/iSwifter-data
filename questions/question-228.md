##### What’s the difference between MKAnnotation and MKPointAnnotation?
***

`MKAnnotation` is a protocol. Typically, we will create a NSObject subclass that implements this protocol. Instances of this custom class will then serve as your map annotation.

`MKPointAnnotation` is a class that implements MKAnnotation. We can use it directly if we want our own business logic on the annotation.
