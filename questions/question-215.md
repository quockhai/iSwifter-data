##### Explain @objc inference
***

We can tag a Swift declaration with **@objc** to indicate that it should be available to Objective-C. In Swift 3 many declarations were automatically inferred to be made available to Objective-C. The most common place for this is any Swift method we want to refer to using a selector.