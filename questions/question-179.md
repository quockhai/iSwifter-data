##### Explain JSONEncoder and JSONDecoder in Swift
***

**Decodable** protocol, which allows us to take data and create instances of our object, populated with the data passed down from the server.

**Encodable** protocol to take instances of our object and turn it into data. With that data, we can store it to the files, send it to the server, whatever you need to do with it.