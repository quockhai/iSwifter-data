##### What is ARC?
***

[ARC](https://docs.swift.org/swift-book/LanguageGuide/AutomaticReferenceCounting.html) is a compile time feature that is Apple’s version of automated memory management. It stands for *Automatic Reference Counting*. This means that it **only** frees up memory for objects when there are **zero strong** references/ to them.