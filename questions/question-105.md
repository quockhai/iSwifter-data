##### Where do we use Dependency Injection?
***

We use a storyboard or xib in our iOS app, then we created IBOutlets. IBOutlet is a property related to a view. These are injected into the view controller when it is instantiated, which is essentially a form of Dependency Injection.

There are forms of dependency injection: constructor injection, property injection and method injection.