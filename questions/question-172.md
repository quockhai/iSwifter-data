##### Explain super keyword in child class
***

We use the `super` keyword to call the parent class initializer after setting the child class stored property.