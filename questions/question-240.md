##### What is the purpose of using IBAction and IBOutlet?
***

`IBAction` and `IBOutlet` are macros defined to denote variables and methods that can be referred to in Interface Builder.

`IBAction` resolves to void and `IBOutlet` resolves to nothing, but they signify to Xcode and Interface builder that these variables and methods can be used in Interface builder to link UI elements to your code.