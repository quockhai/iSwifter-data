##### What is Receipt Validation?
***

The receipt for an application or in-app purchase is a record of the sale of the application and of any in-app purchases made from within the application. You can add receipt validation code to your application to prevent unauthorized copies of your application from running.