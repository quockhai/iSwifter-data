##### What is the difference Non-Escaping and Escaping Closures?
***

The lifecycle of a non-escaping closure is simple:

1. Pass a closure into a function.
2. The function runs the closure (or not).
3. The function returns

**Escaping closure means**, inside the function, you can still run the closure (or not); the extra bit of the closure is stored someplace that will outlive the function. There are several ways to have a closure escape its containing function:

* **Asynchronous execution**: If you execute the closure asynchronously on a dispatch queue, the queue will hold onto the closure for you. You have no idea when the closure will be executed and there’s no guarantee it will complete before the function returns.
* **Storage**: Storing the closure to a global variable, property, or any other bit of storage that lives on past the function call means the closure has also escaped.

for [more details](https://swiftunboxed.com/lang/closures-escaping-noescape-swift3/).