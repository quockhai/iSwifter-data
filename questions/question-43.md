##### What is Concurrency?
***

Concurrency is dividing up the execution paths of your program so that they are possibly running at the same time. The common terminology: process, thread, multithreading, and others. 

Terminology:

* **Process**, an instance of an executing app.
* **Thread**, path of execution for code.
* **Multithreading**, multiple threads or multiple paths of execution running at the same time.
* **Concurrency**, execute multiple tasks at the same time in a scalable manner.
* **Queues**, queues are lightweight data structures that manage objects in the order of First-in, First-out (FIFO).
* **Synchronous** vs **Asynchronous** tasks.