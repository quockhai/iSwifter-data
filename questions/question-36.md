##### How many are there APIs for battery-efficient location tracking?
***

There are 3 APIs:

* **Significant location changes** - the location is delivered approximately every 500 metres (usually up to 1 km)
* **Region monitoring** - track enter/exit events from circular regions with a radius equal to 100m or more. Region monitoring is the most precise API after GPS.
* **Visit events** - monitor place Visit events which are enters/exits from a place (home/office).