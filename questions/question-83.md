##### Explain SiriKit Limitations
***

* SiriKit cannot use all app types.
* Not a substitute for a full app only an extension.
* Siri requires a consistent Internet connection to work.
* Siri service needs to communicate Apple Servers.