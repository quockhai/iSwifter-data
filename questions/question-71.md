##### Explain Forced Unwrapping
***

When we defined a variable as **optional**, then to get the value from this variable, we will have to **unwrap** it. This just means putting an exclamation mark at the end of the variable. The example of the implicitly unwrapped optional type is the IBOutlets we created for your view controller.

We have to use **Forced Unwrapping** when we know an optional has a value.