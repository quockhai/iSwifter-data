##### How to Prioritize Usability in Design?
***

Broke down its design process to prioritize usability in 4 steps:

* Think like the user, then design the UX.
* Remember that users are people, not demographics.
* When promoting an app, consider all the situations in which it could be useful.
* Keep working on the utility of the app even after launch.