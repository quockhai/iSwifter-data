##### What is Factory method pattern?
***

Factory method pattern makes the codebase more flexible to add or remove new types. To add a new type, we just need a new class for the type and a new factory to produce it like the following code. For more information [check this out](https://medium.com/swiftworld/swift-world-design-patterns-factory-method-2be4bb3c73cc).