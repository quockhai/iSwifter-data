##### What is Xcode Bot?
***

Xcode Server uses bots to automatically build your projects. A bot represents a single remote repository, project, and scheme. We can also control the build configuration the bot uses and choose which devices and simulators the bot will use.