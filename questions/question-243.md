##### What is LLDB?
***

It’s the [debugger](https://en.wikipedia.org/wiki/Debugger) of The **[LLVM](http://llvm.org)** Compiler Infrastructure. Here is the [homepage](http://lldb.llvm.org) and this is a good article about it: **[Dancing in the Debugger - A Waltz with LLDB.](https://www.objc.io/issues/19-debugging/lldb-debugging/#lldb)**