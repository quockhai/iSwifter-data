##### How do you follow up clean code for this project?
***

I follow style guide and coding conventions for Swift projects of [Github](https://github.com/github/swift-style-guide) and [SwiftLint](https://github.com/realm/SwiftLint).