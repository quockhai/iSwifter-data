##### What’s the difference between accessibilityLabel and accessibilityIdentifier?
***

`accessibilityLabel` is the value that’s read by VoiceOver to the end-user. As such, this should be a localized string. The text should also be capitalized. Because this helps with VoiceOver’s pronunciation. `accessibilityLabel` is used for testing and Visual Impaired users.

`accessibilityIdentifier` identifies an element via accessibility, but unlike `accessibilityLabel`, `accessibilityIdentifier's` purpose is purely to be used as an identifier for UI Automation tests. We use a value for testing process.