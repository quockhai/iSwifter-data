##### What Is Dependency Management?
***

If we want to integrate open source project, add a framework from a third party project, or even reuse code across our own projects, dependency management helps us to manage these relationships. [Check this out](https://www.raywenderlich.com/690-dependency-management-using-git-submodules).