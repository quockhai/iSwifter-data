##### Explain place holder constraint
***

This tells Interface Builder to go ahead and remove the constraints when we build and run this code. It allows the layout engine to figure out a base layout, and then we can modify that base layout at run time.