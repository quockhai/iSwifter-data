##### What is Protocol?
***

A protocol defines a blueprint of methods, properties and other requirements that suit a particular task or piece of functionality. The protocol can then be adopted by a class, structure, or enumeration to provide an actual implementation of those requirements.

[The Swift Programming Language Guide by Apple](https://docs.swift.org/swift-book/LanguageGuide/Protocols.html)