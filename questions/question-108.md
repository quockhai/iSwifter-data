##### What kind of order functions can we use on collection types?
***

* **map(_:)**: Returns an array of results after transforming each element in the sequence using the provided closure.

* **filter(_:)**: Returns an array of elements that satisfy the provided closure predicate.

* **reduce(\_:_:)**: Returns a single value by combining each element in the sequence using the provided closure.

* **sorted(by:)**: Returns an array of the elements in the sequence sorted based on the provided closure predicate.

To see all methods available from **Sequence**, take a look at the [Sequence docs](swiftdoc.org/v3.0/protocol/Sequence/).