##### Explain Core ML Pros and Cons
***

**Pros:**

* Really easy to add into your app.
* Not just for deep learning: also does logistic regression, decision trees, and other “classic” machine learning models.
* Comes with a handy converter tool that supports several different training packages (Keras, Caffe, scikit-learn, and others).


**Cons:**

* Core ML only supports a limited number of model types. If you trained a model that does something Core ML does not support, then you cannot use Core ML.
* The conversion tools currently support only a few training packages. A notable omission is TensorFlow, arguably the most popular machine learning tool out there. You can write your own converters, but this isn’t a job for a novice. (The reason TensorFlow is not supported is that it is a low-level package for making general computational graphs, while Core ML works at a much higher level of abstraction.)
* No flexibility, little control. The Core ML API is very basic, it only lets you load a model and run it. There is no way to add custom code to your models.
* iOS 11 and later only.

For more [information](http://machinethink.net/blog/machine-learning-apis/).