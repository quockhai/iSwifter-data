##### What’s new with Xcode Server?
***

There are a few improvements:

* Xcode Server no longer need macOS Server app, it’s now built inside Xcode 9.
* Continuous integration bots can be run on any Mac with Xcode 9, no need to install macOS Server.