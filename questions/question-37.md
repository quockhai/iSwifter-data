##### What is the Swift main advantage?
***

To mention some of the main advantages of Swift:

* Optional Types, which make applications crash-resistant.
* Built-in error handling.
* Closures.
* Much faster compared to other languages.
* Type-safe language.
* Supports pattern matching.