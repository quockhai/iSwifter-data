##### What is URLSession?
***

When we want to retrieve contents from a certain URL, we choose to use URLConnection. There are three types of tasks:

* Data tasks: getting data to memory
* Download tasks: downloading file to disk
* Upload tasks: uploading file from disk and receiving response as data in memory