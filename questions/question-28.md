##### What is Observer Pattern?
***

In the Observer pattern, one object notifies other objects of any state changes.

Cocoa implements the observer pattern in two ways: **Notifications** and **Key-Value Observing (KVO).**