##### How many different annotations available in Objective-C?
***

* \_Null_unspecified, which bridges to a Swift implicitly unwrapped optional. This is the default.
* _Nonnull, the value won’t be nil it bridges to a regular reference.
* _Nullable the value can be nil, it bridges to an optional.
* \_Null_resettable the value can never be nil, when read but you can set it to know to reset it. This is only apply property.