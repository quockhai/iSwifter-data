##### What is Encapsulation?
***

Encapsulation is an object-oriented design principles and hides the internal states and functionality of objects. That means objects keep their state information private.