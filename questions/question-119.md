##### When do you use optional chaining vs. if let or guard?
***

We use optional chaining when we do not really care if the operation fails; otherwise, we use `if let` or `guard`. Optional chaining lets us run code only if our optional has a value.

Using the question mark operator like this is called optional chaining. Apple’s [documentation](https://docs.swift.org/swift-book/LanguageGuide/OptionalChaining.html#//apple_ref/doc/uid/TP40014097-CH21-XID_368) explains it like this:

> Optional chaining is a process for querying and calling properties, methods, and subscripts on an optional that might currently be nil. If the optional contains a value, the property, method, or subscript call succeeds; if the optional is nil, the property, method, or subscript call returns nil. Multiple queries can be chained together, and the entire chain fails gracefully if any link in the chain is nil.