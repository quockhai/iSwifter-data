##### Why do we use [availability](https://docs.swift.org/swift-book/ReferenceManual/Attributes.html) attributes?
***

Apple wants to support **one system version back**, meaning that we should support iOS9 or iOS8. [Availability Attributes](raywenderlich.com/908-availability-attributes-in-swift) lets us to support previous version iOS.