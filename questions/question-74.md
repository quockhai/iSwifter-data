##### Explain Swift Standard Library Protocol
***

There are a few different protocol. **[Equatable](https://developer.apple.com/documentation/swift/equatable)** protocol, that governs how we can distinguish between two instances of the same type. That means we can analyze. If we have a specific value is in our array. The **[comparable](https://developer.apple.com/documentation/swift/comparable)** protocol, to compare two instances of the same type and **[sequence](https://developer.apple.com/documentation/swift/sequence)** protocol: `prefix(while:)` and `drop(while:)` [[SE-0045]](https://github.com/apple/swift-evolution/blob/master/proposals/0045-scan-takewhile-dropwhile.md).

Swift 4 introduces a new Codable protocol that lets us serialize and deserialize custom data types without writing any special code.