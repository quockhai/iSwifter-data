##### Why do we need to specify self to refer to a stored property or a method When writing asynchronous code?
***

Since the code is dispatched to a background thread we need to capture a reference to the correct object.