##### Explain View Controller Lifecycle events order
***

There are a few different lifecycle events:

`loadView`
> Creates the view that the controller manages. It’s only called when the view controller is created and only when done programatically. It is responsible for making the view property exist in the first place.

`viewDidLoad`
> Called after the controller’s view is loaded into memory. It’s only called when the view is created.

`viewWillAppear`
> It’s called whenever the view is presented on the screen. In this step the view has bounds defined but the orientation is not applied.

`viewWillLayoutSubviews`
> Called to notify the view controller that its view is about to layout its subviews. This method is called every time the frame changes

`viewDidLayoutSubviews`
> Called to notify the view controller that its view has just laid out its subviews. Make additional changes here after the view lays out its subviews.

`viewDidAppear`
> Notifies the view controller that its view was added to a view hierarchy.

`viewWillDisappear`
> Before the transition to the next view controller happens and the origin view controller gets removed from screen, this method gets called.

`viewDidDisappear`
> After a view controller gets removed from the screen, this method gets called. You usually override this method to stop tasks that are should not run while a view controller is not on screen.

`viewWillTransition(to:with:)`
> When the interface orientation changes, UIKit calls this method on the window’s root view controller before the size changes are about to be made. The root view controller then notifies its child view controllers, propagating the message throughout the view controller hierarchy.