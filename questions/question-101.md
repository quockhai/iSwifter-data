##### What kind of JSONSerialization have ReadingOptions?
***

**mutableContainers**, Specifies that arrays and dictionaries are created as variables objects, not constants.

**mutableLeaves**, Specifies that leaf strings in the JSON object graph are created as instances of variable String.

**allowFragments**, Specifies that the parser should allow top-level objects that are not an instance of Array or Dictionary.