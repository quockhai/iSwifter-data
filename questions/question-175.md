##### What is HealthKit?
***

HealthKit is a framework on iOS. It stores health and fitness data in a central location. It takes in data from multiple sources, which could be different devices. It allows users to control access to their data and maintains privacy of user data. Data is synced between your phone and your watch.