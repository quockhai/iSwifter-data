##### What is the difference open & public access level?
***

**open** allows other modules to use the class and inherit the class; for members, it allows others modules to use the member and override it. For [more information](https://docs.swift.org/swift-book/LanguageGuide/AccessControl.html).

**public** only allows other modules to use the public classes and the public members. Public classes can no longer be subclassed, nor public members can be overridden.