##### Explain [weak self] and [unowned self]
***

**unowned (non-strong reference)** does the same as weak with one exception: The variable will not become nil and must not be optional.

When you try to access the variable after its instance has been deallocated. That means, you should only use **unowned** when you are sure, that this variable will never be accessed after the corresponding instance has been deallocated.

However, if you don’t want the variable to be weak AND you are sure that it can’t be accessed after the corresponding instance has been deallocated, you can use unowned.

* Every time used with non-optional types.
* Every time used with let.

By declaring it **[weak self]** you get to handle the case that it might be nil inside the closure at some point and therefore the variable must be an optional. A case for using **[weak self]** in an asynchronous network request, is in a **view controller** where that request is used to populate the view.