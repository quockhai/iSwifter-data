##### What problems does delegation solve?
***

* Avoiding tight coupling of objects.
* Modifying behavior and appearance without the need to subclass objects.
* Allowing tasks to be handed off to any arbitrary object.