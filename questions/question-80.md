##### What is the major purposes of Frameworks?
***

Frameworks have three major purposes:

* Code encapsulation
* Code modularity
* Code reuse

You can share your framework with your other apps, team members, or the iOS community. When combined with Swift’s access control, frameworks help define strong, testable interfaces between code modules.