##### What’s the difference optional between nil and .None?
***

There is no difference. `Optional.None` (`.None` for short) is the correct way of initializing an optional variable lacking a value, whereas `nil` is just syntactic sugar for `.None`. Check this [out](https://www.raywenderlich.com/762435-swift-interview-questions-and-answers).
