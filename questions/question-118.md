##### What is Hashable?
***

[Hashable](https://developer.apple.com/documentation/swift/hashable) allows us to use our objects as keys in a dictionary. So we can make our custom types that can be compared for its equality using it’s `hashValue`.