##### REST, HTTP, JSON - What’s that?
***

**HTTP** is the application protocol or set of rules, websites use to transfer data from the web server to client. The client (your web browser or app) use to indicate the desired action:

* GET: Used to retrieve data, such as a web page, but doesn’t alter any data on the server.
* HEAD: Identical to GET but only sends back the headers and none of the actual data.
* POST: Used to send data to the server, commonly used when filling a form and clicking submit.
* PUT: Used to send data to the specific location provided.
* DELETE: Deletes data from the specific location provided.

**REST**, or REpresentational State Transfer, is a set of rules for designing consistent, easy-to-use and maintainable web APIs.

**JSON** stands for JavaScript Object Notation; it provides a straightforward, human-readable and portable mechanism for transporting data between two systems. Apple supplies the `JSONSerialization` class to help convert your objects in memory to JSON and vice-versa.