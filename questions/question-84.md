##### Why do we use a delegate pattern to be notified of the text field’s events?
***

Because at most only a single object needs to know about the event.