##### Explain UIBezierPath
***

`UIBezierPath` class allows us define custom paths that describe any shape, and use those paths to achieve any custom result we want.