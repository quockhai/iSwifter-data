##### What does Yak Shaving mean?
***

**Yak Shaving** is a programming term that refers to a series of tasks that need to be performed before a project can progress to its next milestone.