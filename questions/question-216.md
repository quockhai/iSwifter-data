##### What is Safe area?
***

**Safe area** allows us to create constraints to keep our content from being hidden by UIKit bars like the status, navigation or tab bar. Previously we used `topLayoutGuide` and `bottomLayoutGuide`.