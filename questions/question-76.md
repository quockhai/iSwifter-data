##### What is the difference CollectionViews & TableViews?
***

**TableViews** display a list of items, in a single column, a vertical fashion, and limited to vertical or horizontal scrolling only.

**CollectionViews** also display a list of items, however, they can have multiple columns and rows.