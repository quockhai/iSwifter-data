##### Explain Readers-Writers
***

Multiple threads reading at the same time while there should be only one thread writing. The solution to the problem is a readers-writers lock which allows concurrent read-only access and an exclusive write access.

Terminology:

* **Race Condition**, a race condition occurs when two or more threads can access shared data and they try to change it at the same time.
* **Deadlock**, a deadlock occurs when two or sometimes more tasks wait for the other to finish, and neither ever does.
* **Readers-Writers problem**, multiple threads reading at the same time while there should be only one thread writing.
* **Readers-writer lock**, such a lock allows concurrent read-only access to the shared resource while write operations require exclusive access.
* **Dispatch Barrier Block**, dispatch barrier blocks create a serial-style bottleneck when working with concurrent queues.