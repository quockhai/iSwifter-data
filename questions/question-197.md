##### Explain Property Observer
***

A [property observer](https://developer.apple.com/library/archive/referencelibrary/GettingStarted/DevelopiOSAppsSwift/GlossaryDefinitions.html#//apple_ref/doc/uid/TP40015214-CH12-SW123) observes and responds to changes in a property’s value. With property observer, we don’t need to reset the controls, every time attributes change.