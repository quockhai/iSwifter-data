##### What is big-o notation?
***

An algorithm is an impression method used to determine the working time for an input **N** size. The big-o notation grade is expressed by the highest value. And the **big-o notation** is finding the answer with the question of **O(n)**. Here is a **[cheat sheet](https://www.bigocheatsheet.com/)** and **[Swift Algorithm Club](https://github.com/raywenderlich/swift-algorithm-club)**. For example

> For Loops big-o notation is O(N). Because For Loops work n times.

> Variables (var number:Int = 4) big-o notation is O(1).