##### What is Class?
***

A **class** is meant to define an object and how it works. In this way, a **class** is like a blueprint of an object.