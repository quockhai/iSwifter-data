##### How could you set up Live Rendering?
***

The attribute *@IBDesignable* lets Interface Builder perform live updates on a particular view. IBDesignable requires Init frame to be defined as well in UIView class.