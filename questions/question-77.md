##### What is Alamofire doing?
***

**Alamofire** uses URL Loading System in the background, so it does integrate well with the Apple-provided mechanisms for all the network development. This means, It provides chainable request/response methods, JSON parameter and response serialization, authentication, and many other features. It has thread mechanics and executes requests on a background thread and call completion blocks on the main thread.