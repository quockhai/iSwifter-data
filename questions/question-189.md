##### What is Strategy Pattern?
***

Strategy pattern allows you to change the behaviour of an algorithm at run time. Using interfaces, we are able to define a family of algorithms, encapsulate each one, and make them interchangeable, allowing us to select which algorithm to execute at run time. For more information [check this out](https://medium.com/design-patterns-in-swift/design-patterns-in-swift-strategy-pattern-1d11945b4adc).