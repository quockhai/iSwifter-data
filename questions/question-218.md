##### Explain Content offset
***

When we scroll a scrollView, it modifies a property known as content offset. Content offset is a point at which the origin of the contentView, that is the bounds rectangle, is offset from the origin of the scrollView.

Using this value, the scrollView can compute its new bounds and redraw any of its subviews.