##### How to pass data between view controllers?
***

There are 3 ways to pass data between view controllers:

1. Segue, in prepareForSegue method (Forward).
2. Delegate (Backward).
3. Setting variable directly (Forward).