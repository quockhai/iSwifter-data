##### What is the biggest changes in UserNotifications?
***

* We can add audio, video and images.
* We can create custom interfaces for notifications.
* We can manage notifications with interfaces in the notification center.
* New Notification extensions allow us to manage remote notification payloads before they’re delivered.