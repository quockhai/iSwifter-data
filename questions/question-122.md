##### Explain to using Class and Inheritance benefits
***

* With Overriding provides a mechanism for customization
* Reuse implementation
* Subclassing provides reuse interface
* Modularity
* Subclasses provide dynamic dispatch