##### How can you prevent a user from doing said action more than once on their device?
***

Apple has introduced [DeviceCheck](https://developer.apple.com/documentation/devicecheck) in iOS 10. This API allows us to access per-device, per-developer data in an iOS device. The solution is better than UserDefaults or Advertising Identifier. DeviceCheck allows us to store a boolean value.