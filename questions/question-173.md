##### Explain Polymorphism
***

Polymorphism is the ability of a class instance to be substituted by a class instance of one of its subclasses.