##### What is @dynamic in Objective-C?
***

We use dynamic for subclasses of NSManagedObject. **@dynamic** tells the compiler that getter and setters are implemented somewhere else.