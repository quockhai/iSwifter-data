##### What is Functional programming?
***

An approach of solving problems by decomposing complicated processes. The goal is avoid changing state or mutating values outside of its scope. There are three main concepts. These concepts are: separating functions and data, immutability, and first-class functions.