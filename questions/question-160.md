##### What’s the difference between Android and iOS designs?
***

* Android uses icons, iOS mostly uses text as their action button. When we finish an action, on Android you will see a checkmark icon, whereas on iOS you’ll have a ‘Done’ or ‘Submit’ text at the top.
* iOS uses subtle gradients, Android uses flat colors and use different tone/shades to create dimension. iOS uses bright, vivid colors — almost neon like. Android is colorful, but not as vivid.
* iOS design is mostly flat, but Android’s design is more dimensional. Android adds depth to their design with floating buttons, cards and so on.
* iOS menu is at the bottom, Android at the top. Also a side menu/hamburger menu is typically Android. iOS usually has a tab called ‘More’ with a hamburger icon, and that’s usually the menu & settings rolled into one.
* Android ellipsis icon (…) to show more options is vertical, iOS is horizontal.
* Android uses Roboto and iOS uses San Francisco font
* Frosted glass effect used to be exclusive to iOS, but you’ll see a lot of them in Android too nowadays. As far as I know it’s still a hard effect to replicate in Android.
* Android usually has navigation button in color, whereas iOS is usually either white or gray.