##### Explain Dynamic Type
***

Dynamic Type allows our app’s text to increase or decrease size on user’s preference improving visibility and accessibility. When the user selects one of the large accessibility sizes from setting menu. We can observe the content size category did change notification, but since iOS 10 there is a better way using trait collections *UITraitCollection*.