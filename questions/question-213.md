##### What is offset?
***

If we want to arrange the button at Bottom-Right of superview with spacing 20 Pts, we use view attribute of UIButton or UILabel for reference to view attribute of superview and use .offset(x) for set padding.