##### Explain MVC
***

**Models** - responsible for the domain data or a data access layer which manipulates the data, think of ‘Person’ or ‘PersonDataProvider’ classes.

**Views** - responsible for the presentation layer (GUI), for iOS environment think of everything starting with ‘UI’ prefix.

**Controller/Presenter/ViewModel** - the glue or the mediator between the Model and the View, in general responsible for altering the Model by reacting to the user’s actions performed on the View and updating the View with changes from the Model.