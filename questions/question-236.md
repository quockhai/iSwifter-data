##### Explain CAShapeLayer
***

`CAShapeLayer` is a CALayer subclass, its provides hardware-accelerated drawing of all sorts of 2D shapes, and includes extra functionality such as fill and stroke colors, line caps, patterns and more. Check out for [more details](https://www.calayer.com/core-animation/2016/05/22/cashapelayer-in-depth.html).