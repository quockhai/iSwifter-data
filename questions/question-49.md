##### Explain Guard statement
***

There are three big benefits to guard statement:

1. One is avoiding the pyramid of doom, as others have mentioned — lots of annoying if let statements nested inside each other moving further and further to the right.
2. The second benefit is providing an early exit out of the function using break or using return.
3. The last benefit, guard statement is another way to safely unwrap optionals.