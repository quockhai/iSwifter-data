##### What is DispatchGroup?
***

*`DispatchGroup` allows for aggregate synchronization of work. We can use them to submit multiple different work items and track when they all complete, even though they might run on different queues. This behavior can be helpful when progress can’t be made until all of the specified tasks are complete.* — [Apple’s Documentation](https://developer.apple.com/documentation/dispatch/dispatchgroup)

The most basic answer: If we need to wait on a couple of asynchronous or synchronous operations before proceeding, we can use `DispatchGroup`.