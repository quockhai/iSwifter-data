##### Explain Main Thread Checker
***

The Main Thread Checker is a new tool launched with Xcode 9 which detects the invalid use of Apple’s frameworks like UIKit, AppKit etc that supposed to be used from main thread but accidentally used in the background thread. The effect of the invalid usage can result in missed UI updates, visual defects, data corruption, and crashes. You can read more about the Main Thread Checker [here](https://developer.apple.com/documentation/code_diagnostics/main_thread_checker).