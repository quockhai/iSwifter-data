##### What is the difference strong, weaks, read only and copy?
***

`strong, weak, assign` property attributes define how memory for that property will be managed.

**Strong** means that the reference count will be increased and the reference to it will be maintained through the life of the object

**Weak ( non-strong reference )**, means that we are pointing to an object but not increasing its reference count. It’s often used when creating a parent child relationship. The parent has a strong reference to the child but the child only has a weak reference to the parent.

* Every time used on var
* Every time used on an optional type
* Automatically changes itself to nil

**Read-only**, we can set the property initially but then it can’t be changed.

**Copy** means that we’re copying the value of the object when it’s created. Also prevents its value from changing.

for [more details](https://www.sm-cloud.com/interview-questions-what-are-properties-and-instance-variables-in-objective-c-and-swift/) check this out.