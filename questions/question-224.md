##### What is NSLayoutAnchor?
***

With iOS 9, Apple introduced the `NSLayoutAnchor` class to make writing autolayout easier with code.

There are three subclasses of **NSLayoutAnchor**:

* **NSLayoutXAxisAnchor** This subclass is used to create horizontal constraints
* **NSLayoutYAxisAnchor** This subclass is used to create vertical constraints
* **NSLayoutDimension** This subclass is used to create the width and height constraints