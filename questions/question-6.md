##### Why don’t we use strong for enum property in Objective-C?
***

Because enums aren’t objects, so we don’t specify strong or weak here.