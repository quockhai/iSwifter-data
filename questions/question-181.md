##### What is NotificationCenter?
***

**NotificationCenter** is an observer pattern, The **NSNotificationCenter** singleton allows us to broadcast information using an object called **NSNotification**.

The biggest difference between **KVO** and **NotificationCenter** is that KVO tracks specific changes to an object, while **NotificationCenter** is used to track generic events.