##### Explain [ObjectMapper](https://github.com/tristanhimmelman/ObjectMapper) for parsing JSON data
***

ObjectMapper converts JSON data to strongly typed model objects. It has a two-way binding between JSON and deal with generic object and nested objects. Also we can manage subclasses.