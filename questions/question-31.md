##### Explain MVVM
***

UIKit independent representation of your View and its state. The View Model invokes changes in the Model and updates itself with the updated Model, and since we have a binding between the View and the View Model, the first is updated accordingly.

Your view model will actually take in your model, and it can format the information that’s going to be displayed on your view.

There is a more known framework called [RxSwift](https://github.com/ReactiveX/RxSwift). It contains RxCocoa, which are reactive extensions for Cocoa and CocoaTouch.