##### Explain CAEmitterLayer and CAEmitterCell
***

UIKit provides two classes for creating particle effects: **CAEmitterLayer** and **CAEmitterCell**.

The **CAEmitterLayer** is the layer that emits, animates and renders the particle system.

The **CAEmitterCell** represents the source of particles and defines the direction and properties of the emitted particles.