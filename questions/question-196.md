##### How to find the distance between two points (x1, y1) and (x2, y2)?
***

We need to calculate the distance between the points, we can omit the sqrt() which will make it a little faster. This question’s background is [Pythagorean theorem](https://en.wikipedia.org/wiki/Pythagorean_theorem). We can find the result with [CGPoint](https://stackoverflow.com/questions/1906511/how-to-find-the-distance-between-two-cg-points/1906659#1906659).