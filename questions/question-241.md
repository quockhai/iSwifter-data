##### Explain [AlamoFire](https://github.com/Alamofire/Alamofire) Benefits
***

* AlamoFire creates a route. This means we can create the request and execute it to the server by one static function.
* AlamoFire provides method chaining for the requests that’s returned, which makes it easy for adding headers,and handling responses.
* AlamoFire has multiple response handlers hat’s returned in binary form, text, parse JSON, and we can even use multiple of these for a given request.
* AlamoFire has the method chaining allows for response validation. We can call validation to check for the status code of the HTTP response, the content type, or any custom validation you might need to do for our app.
* AlamoFire gives us that use a couple of protocols, URLConvertible, and URLRequestConvertible. These protocols can be passed in when creating a request.
* AlamoFire provides extensions can be passed in to create the request.