##### When is a good time for dependency injection in our projects?
***

There is a few guidelines that you can follow.

**Rule 1.** Is Testability important to us? If so, then it is essential to identify external dependencies within the class that you wish to test. Once dependencies can be injected we can easily replace real services for mock ones to make it easy to testing easy.

**Rule 2.** Complex classes have complex dependencies, include application-level logic, or access external resources such as the disk or the network. Most of the classes in your application will be complex, including almost any controller object and most model objects. The easiest way to get started is to pick a complex class in your application and look for places where you initialize other complex objects within that class.

**Rule 3.** If an object is creating instances of other objects that are shared dependencies within other objects then it is a good candidate for a dependency injection.