##### Explain types of notifications
***

There are two type of notifications: **Remote** and **Local**. Remote notification requires connection to a server. Local notifications don’t require server connection. Local notifications happen on device.