##### Explain Priority Inversion and Priority Inheritance
***

If high priority thread waits for low priority thread, this is called **[Priority Inversion](https://en.wikipedia.org/wiki/Priority_inversion)**. if low priority thread temporarily inherit the priority of the highest priority thread, this is called **[Priority Inheritance](https://en.wikipedia.org/wiki/Priority_inheritance)**.