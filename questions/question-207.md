##### Explain IteratorProtocol
***

The `IteratorProtocol` protocol is tightly linked with the `Sequence` protocol. Sequences provide access to their elements by creating an iterator, which keeps track of its iteration process and returns one element at a time as it advances through the sequence. There are very good [examples](https://developer.apple.com/documentation/swift/iteratorprotocol) on the differences of `IteratorProtocol` from for loop and while loop.