##### What is interface?
***

The **@interface** in Objective-C has nothing to do with Java interfaces. It simply declares a public interface of a class, its public API.