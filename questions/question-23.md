##### Why is design pattern very important?
***

Design patterns are reusable solutions to common problems in software design. They’re templates designed to help you write code that’s easy to understand and reuse. Most common Cocoa design patterns:

* **Creational**: Singleton.
* **Structural**: Decorator, Adapter, Facade.
* **Behavioral**: Observer, and, Memento