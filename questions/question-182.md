##### Why is inheritance bad in Swift?
***

* We cannot use superclasses or inheritance with Swift value types.
* Upfront modelling
* Customization for inheritance is an imprecise