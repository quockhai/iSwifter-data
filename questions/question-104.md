##### What is RGR ( Red — Green — Refactor)?
***

Red, Green and Refactor are stages of the TDD (Test Driven Development).

1. **Red**: Write a small amount of test code usually no more than seven lines of code and watch it fail.
2. **Green**: Write a small amount of production code. Again, usually no more than seven lines of code and make your test pass.
3. **Refactor**: Tests are passing, you can make changes without worrying. Clean up your code. There are great workshop notes [here](https://gist.github.com/orkoden/201f811570582fab2b5b).