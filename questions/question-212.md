##### What is OAuth?
***

OAuth short for *Open Authorization* is an authorization protocol and not one used for authentication. OAuth being an authorization protocol, isn’t concerned with identifying the user. Instead, it deals with the authorization of a third party application to access user data without exposing the user’s credentials. There are two libraries: [OAuthSwift](https://github.com/OAuthSwift/OAuthSwift) & [OAuth2](https://github.com/p2/OAuth2)