##### What is JSON/PLIST limits?
***

* We create your objects and then serialized them to disk.
* It’s great and very limited use cases.
* We can’t obviously use complex queries to filter your results.
* It’s very slow.
* Each time we need something, we need to either serialize or deserialize it.
* It’s not thread-safe.