##### What is the difference fileprivate, private and public private(set) access level?
***

**fileprivate** is accessible within the current file, **private** is accessible within the current declaration.

**public private(set)** means getter is public, but the setter is private.