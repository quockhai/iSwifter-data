##### What is bitcode?
***

**[Bitcode](https://stackoverflow.com/questions/30722606/what-does-enable-bitcode-do-in-xcode-7)** refers to to the type of code: “LLVM Bitcode” that is sent to iTunes Connect. This allows Apple to use certain calculations to re-optimize apps further (e.g: possibly downsize executable sizes). If Apple needs to alter your executable then they can do this without a new build being uploaded.