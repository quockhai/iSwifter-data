##### How to educate app with Context?
***

Education in context technique helping users interact with an element or surface in a way they have not done so previously. This technique often includes *slight visual cues* and *subtle animation*.