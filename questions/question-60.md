##### What is the three major debugging improvements in Xcode 8?
***

* The **View Debugger** lets us visualize our layouts and see constraint definitions at runtime. Although this has been around since Xcode 6, Xcode 8 introduces some handy new warnings for constraint conflicts and other great convenience features.
* The **Thread Sanitizer** is an all new runtime tool in Xcode 8 that alerts you to threading issues - most notably, potential race conditions.
* The **Memory Graph Debugger** is also brand new to Xcode 8. It provides visualization of your app’s memory graph at a point in time and flags leaks in the Issue navigator.