##### What is iOS 11 SDK Features for Developers?
***

* New MapKit Markers
* Configurable File Headers
* Block Based UITableView Updates
* MapKit Clustering
* Closure Based KVO
* Vector UIImage Support
* New MapKit Display Type
* Named colors in Asset Catalog
* Password Autofill
* Face landmarks, Barcode and Text Detection
* Multitasking using the new floating Dock, slide-over apps, pinned apps, and the new App Switcher
* Location Permission: A flashing blue status bar anytime an app is collecting your location data in the background. Updated locations permissions that always give the user the ability to choose only to share location while using the app.

for [more information](https://mackuba.eu/2017/07/05/new-stuff-from-wwdc-2017/).