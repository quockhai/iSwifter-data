##### Explain @objcMembers inference
***

When we declare this class as having @objcMembers everything in the class will be exposed to the Objective-C runtime. This is the same as writing implicitly @objc in front of the function.