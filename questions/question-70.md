##### What is five essential practical guidelines to improve your typographic quality of mobile product designs?
***

1. Start by choosing your body text typeface.
2. Try to avoid mixing typefaces.
3. Watch your line length.
4. Balance line height and point size.
5. Use proper Apostrophes and Dashes.