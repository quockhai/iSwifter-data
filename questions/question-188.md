##### Explain .gitignore
***

.gitignore is a file extension that you tell Git server about document types, and folders that you do not want to add to the project or do not want to track changes made in Git server projects.