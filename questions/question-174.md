##### Explain In-app Purchase products and subscriptions
***

* **Consumable products**: can be purchased more than once and used items would have to re-purchase.
* **Non-consumable products**: user would be able to restore this functionality in the future, should they need to reinstall the app for any reason. We can also add subscriptions to our app.
* **Non-Renewing Subscription**: Used for a certain amount of time and certain content.
* **Auto-Renewing Subscription**: Used for recurring monthly subscriptions.