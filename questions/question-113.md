##### What is the difference Filter and Map Function?
***

**Map**, we pass in a function that returns a value for each element in an array. The return value of this function represents what an element becomes in our new array.

**Filter**, we pass in a function that returns either true or false for each element. If the function that we pass returns true for a given element, then the element is included in the final array.