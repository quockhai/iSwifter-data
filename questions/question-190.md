##### What is an “app ID” and a “bundle ID”?
***

A bundle ID is the identifier of a single app. For example, if your organization’s domain is xxx.com and you create an app named Facebook, you could assign the string com.xxx.facebook as our app’s bundle ID.

An App ID is a two-part string used to identify one or more apps from a single development team. You need Apple Developer account for an App ID.