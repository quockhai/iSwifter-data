##### Explain XLIFF Pros and Cons
***

**Pros**

* It helps us to extract the localization work from or code so we don’t have to make any language assumptions in your source code.
* XLIFF files also holds the content to be localized from our development language along with the translations.
* XLIFF consolidates strings from different resources and different file types into one single document.

**Cons**

* XLIFF does not give them that visual and functional context.
* XLIFF does not provide resource data like assets in your project.
* XLIFF does not provide with the custom metadata about the generated XLIFF.