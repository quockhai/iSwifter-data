##### What is [Continuous Integration](https://www.thoughtworks.com/continuous-integration)?
***

Continuous Integration allows us to get early feedback when something is going wrong during application development. There are a lot of continuous integration tools available.

**Self-hosted server**

* [Xcode Server](https://developer.apple.com/library/archive/documentation/IDEs/Conceptual/xcode_guide-continuous_integration/)
* [Jenkins](https://jenkins.io/)
* [TeamCity](https://www.jetbrains.com/teamcity/)

**Cloud solutions**

* [TravisCI](https://travis-ci.org/)
* [Bitrise](https://www.bitrise.io/)
* [Buddybuild](https://www.buddybuild.com/)