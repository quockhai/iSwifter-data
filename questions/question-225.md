##### What is themutating keyword?
***

The `mutating` keyword is used to let variables be modified in a `struct` or `enum`. If we need to modify the properties of a value type, we have to use the **mutating keyword** in the instance method.