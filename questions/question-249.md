##### What is the meaning of id?
***

`id` is a pointer to any type, it always points to an Objective-C object. The `AnyObject` protocol is similar and it helps bridge the gap between Swift and Objective-C.