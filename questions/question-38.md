##### Explain generics in Swift?
***

Generics create code that does not get specific about underlying data types. Don’t catch this [article](https://www.raywenderlich.com/3535703-swift-generics-tutorial-getting-started). Generics allow us to know what type it is going to contain. Generics also provides optimization for our code.