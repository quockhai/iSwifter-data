##### What is the difference SVN and Git?
***

**SVN** relies on a centralised system for version management. It’s a central repository where working copies are generated and a network connection is required for access.

**Git** relies on a distributed system for version management. You will have a local repository on which you can work, with a network connection only required to synchronise.