##### Explain Arrange-Act-Assert
***

**AAA** is a pattern for arranging and formatting code in Unit Tests. If we were to write XCTests each of our tests would group these functional sections, separated by blank lines:

* **Arrange** all necessary preconditions and inputs.
* **Act** on the object or method under test.
* **Assert** that the expected results have occurred.