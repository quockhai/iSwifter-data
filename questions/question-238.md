##### Explain coordinate systems in views
***

UIkit defines a default coordinate system with the origin in the top left corner, and axis extending to the right, and down from the origin point. Views are laid out within this coordinate system to position and size them.

In addition to this Default Coordinate System, which we’ll call the Screen Coordinate System, an app’s window and views also define their own Local Coordinate System.

For example single view, a view object tracks its size and location using a frame and bounds. A frame is a rectangle, which specifies the size and location of the view within its SuperView coordinate system.

A bounds rectangle, on the other hand, specifies the size of the view within its own local coordinate system.