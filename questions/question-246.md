##### Explain Dependency Inversion Principle
***

Dependency Inversion Principle serves to decouple classes from one another by explicitly providing dependencies for them, rather than having them create the dependencies themselves.