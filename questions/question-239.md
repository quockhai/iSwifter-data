##### Explain Encoding, Decoding and Serialization, Deserialization
***

**Serialization** is the process of converting data into a single string or json so it can be stored or transmitted easily. **Serialization**, also known as **encoding**. The reverse process of turning a single string into a data is called **decoding**, or **deserialization**. In swift we use **Codable** protocol that a type can conform to, to declare that it can be encoded and decoded. It’s basically an alias for the **Encodable** and **Decodable** protocols.