##### Explain Queues
***

Queues are used to store a set of data, but are different in that the first item to go into this collection, will be the first item to be removed. Also well known as FIFO which means, ‘first in first out’.