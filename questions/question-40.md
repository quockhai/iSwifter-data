##### Explain what is defer
***

`defer` keyword which provides a block of code that will be executed in the case when execution is leaving the current scope.