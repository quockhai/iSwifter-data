##### Explain CodingKey Protocol
***

The **CodingKeys** enum **(Protocol)** lets you rename specific properties in case the serialized format doesn’t match the requirements of the API. CodingKeys should have nested enum.