##### Explain Swift Package Manager
***

The Swift Package Manager will help to vastly improve the Swift ecosystem, making Swift much easier to use and deploy on platforms without Xcode such as Linux. The Swift Package Manager also addresses the problem of [dependency hell](https://en.wikipedia.org/wiki/Dependency_hell) that can happen when using many interdependent libraries.

The Swift Package Manager only supports using the master branch. Swift Package Manager now supports packages with Swift, C, C++ and Objective-C.