##### Explain how does UIEdgeInsetsMake work?
***

According to the documentation: We can use this method to add cap insets to an image or to change the existing cap insets of an image. In both cases, you get back a new image and the original image remains untouched. We use the amount of pixels what we want to make unstretchable in the values of the `UIEdgeInsetsMake` function. Goal is to keep original rounded corners of image. With UIEdgeInsets, we can specify how many pixels to the top, left, bottom, and right stretching the image.

Syntax : `UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)`