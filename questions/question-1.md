##### What is the difference between Synchronous & Asynchronous task?
***

* **Synchronous**: waits until the task have completed.
* **Asynchronous**: completes a task in the background and can notify you when complete.