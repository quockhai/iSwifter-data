##### Explain Grand Central Dispatch (GCD)
***

GCD is a library that provides a low-level and object-based API to run tasks concurrently while managing threads behind the scenes.

Terminology:

* **Dispatch Queues**, a dispatch queue is responsible for executing a task in the First-In, First-Out order.
* **Serial Dispatch Queue**, a serial dispatch queue runs tasks one at a time.
* **Concurrent Dispatch Queue**, a concurrent dispatch queue runs as many tasks as it can without waiting for the started tasks to finish.
* **Main Dispatch Queue**, a globally available serial queue that executes tasks on the application’s main thread.