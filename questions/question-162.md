##### Explain difference dependency injection and inject dependencies
***

Using dependency injection for a view model or view controller is easy enough, but to inject dependencies into every view on-screen would be significant work and a lot more lines of code to manage.