##### Why do we use synchronized?
***

synchronized guarantees that only one thread can be executing that code in the block at any given time.